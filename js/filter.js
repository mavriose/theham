$(function() {
    let selectedClass = "";
    $(".portfolio-btn").click(function(){
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(100, 0.1);
        $("#portfolio .item").not("."+selectedClass).fadeOut().removeClass('scale-anm');

        setTimeout(function() {
            $("."+selectedClass).fadeIn().addClass('scale-anm');
            $("#portfolio").fadeTo(300, 1);
        }, 300);

    });
});
