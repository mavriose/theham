$(document).ready(function () {
    $(".tabs_caption li").click(function () {
        switchTabs(this, `active`, `tabs-content`);
    })
});

function switchTabs(elem, switchClass, contentClass) {
    $(elem).addClass(switchClass).siblings().removeClass(switchClass);
    const tabIndex = $(elem).index();
    $(`.${contentClass}`).removeClass(switchClass).eq(tabIndex).addClass(switchClass)
}

$(".toolbar").click(function (e) {
    $(".active-filter").removeClass("active-filter");
    $(e.target).addClass("active-filter");
});

let btn = document.getElementsByClassName('load-more-btn');
btn[0].addEventListener('click', displayImg);

function displayImg() {
    const img = document.getElementById('hidden-img');
    img.style.display = 'flex';
    btn[0].style.display = 'none';


}


$('.rev-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.rev-slider-nav',
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'linear'
});
$('.rev-slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.rev-slider',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0',
    cssEase: 'linear'
});

$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 25,
    gutter: 10
});





